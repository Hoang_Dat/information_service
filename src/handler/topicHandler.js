const Topic = require("../model/topic");

const insertTopic = async data => {
    const newTopic = new Topic(data);
    return await newTopic.save();
};

const getAllTopics = async data => {
    return await Topic.find();
};

const deleteTopic = async (id) => {
    return await Topic.findOneAndDelete({ _id: id});
  };

module.exports = {
    insertTopic,
    getAllTopics,
    deleteTopic
  };
const Place = require("../model/place");

const insertPlace = async data => {
    const newPlace = new Place(data);
    return await newPlace.save();
};

const getAllPlaces = async () => {
    return await Place.find();
}; 

const getDetailPlaceById = async id => {
  return await Place.findById(id);
};

const deletePlace = async (id) => {
    return await Place.findOneAndDelete({ _id: id});
  };

const updatePlace = async (id, place) => {
    return await Place.findByIdAndUpdate(id, place, {new: true});
  };

module.exports = {
    insertPlace,
    getAllPlaces,
    getDetailPlaceById,
    deletePlace,
    updatePlace
  };
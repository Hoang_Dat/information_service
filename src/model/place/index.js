const mongoose = require("mongoose");

module.exports = mongoose.model("Place", require("./schema"));
const Schema = require("mongoose").Schema;

const PlaceSchema = new Schema({
    lat:  { type: Number, default : 0 },
    long: { type: Number, default: 0 },
    address: {
        province_city: String,
        district: String,
        street: String
      },
    title: { type: String, required: true },
    content: { type: String },
    imageView: { type: String },
    likes: { type: Number, default: 0 }
  });
  
  module.exports = PlaceSchema;
const mongoose = require("mongoose");

module.exports = mongoose.model("Topic", require("./schema"));
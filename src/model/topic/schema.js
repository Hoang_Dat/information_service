const Schema = require("mongoose").Schema;

const TopicSchema = new Schema({
  name: { type: String, required: true },
  description : { type : String}
});

module.exports = TopicSchema;
const { insertTopic,
    getAllTopics,
    deleteTopic} = require('../handler/topicHandler');
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const getListTopic = async (call, callback) => {
    const listTopics = await getAllTopics()
        if (listTopics) {
            callback(null, { topics : listTopics})
        } else {
            callback({
                code: grpc.status.INTERNAL,
                details: "get topics faillure"
            })
        }
}
const addTopic = async (call, callback) => {
    console.log(call)
    const data = {
        name : call.request.name,
        description : call.request.description
    }
    const result = await insertTopic(data);
    callback(null, result)
}

const removeTopic = async (call, callback) => {
    const result = await deleteTopic(call.request.id);
    callback(null, result)
}

module.exports = {
    getListTopic,
    addTopic,
    removeTopic
};
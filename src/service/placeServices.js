const { insertPlace,
    getAllPlaces,
    getDetailPlaceById,
    deletePlace,
    updatePlace} = require('../handler/placeHandler');
const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const getListPlace = async (_, callback) => {
    const places = await getAllPlaces();
        if (places) {
            callback(null, {places : places})
        } else {
            callback({
                code: grpc.status.INTERNAL,
                details: "get places faillure"
            })
        }
}
const getPlaceById = async (call, callback) => {
    const place = await getDetailPlaceById(call.request.id)
        if (place) {
            callback(null, place)
        } else {
            callback({
                code: grpc.status.INTERNAL,
                details: "get detail place faillure"
            })
        }
}
const addPlace = async (call, callback) => {
    const data = {
        lat : call.request.lat,
        long : call.request.long,
        address : {
            province_city : call.request.province_city,
            district : call.request.district,
            street : call.request.street
        },
        title : call.request.title,
        content : call.request.content,
        imageView : call.request.imageView,
        likes : call.request.likes,
    }
    const result = await insertPlace(data);
    callback(null, result)
}

const removePlace = async (call, callback) => {
    const result = await deletePlace(call.request.id);
    callback(null, result)
}

const editPlace = async (call, callback) => {
    const result = await updatePlace(call.request.id, call.request.place);
    callback(null, result)
}

module.exports = {
    getListPlace,
    addPlace,
    getPlaceById,
    removePlace,
    editPlace
};
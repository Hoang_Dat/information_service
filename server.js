const appointmentDB = require('./src/config/database');
const grpc = require('grpc');
const {
    getListPlace,
    addPlace,
    removePlace,
    editPlace,
    getPlaceById
} = require('./src/service/placeServices');
const {
    getListTopic,
    addTopic,
    removeTopic
} = require('./src/service/topicServices');

const PROTO_PATH =__dirname + '/src/proto/information.proto';
const protoLoader = require('@grpc/proto-loader');
const packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });
const information_proto = grpc.loadPackageDefinition(packageDefinition);
console.log(information_proto)
const server = new grpc.Server();
server.addService(information_proto.InformationService.service, {
    InsertPlace: addPlace,
    ListPlace: getListPlace,
    GetPlaceById: getPlaceById,
    DeletePlace: removePlace,
    UpdatePlace: editPlace,
    ListTopic: getListTopic,
    InsertTopic: addTopic,
    DeleteTopic: removeTopic,
})

server.bind('127.0.0.1:50057',
    grpc.ServerCredentials.createInsecure());
console.log('Server running at http://127.0.0.1:50057')
server.start()